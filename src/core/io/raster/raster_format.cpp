#include "raster_format.hpp"
#include "io/raster/raster_mime.hpp"

glaxnimate::io::Autoreg<glaxnimate::io::raster::RasterMime> glaxnimate::io::raster::RasterMime::autoreg;
glaxnimate::io::Autoreg<glaxnimate::io::raster::RasterFormat> glaxnimate::io::raster::RasterFormat::autoreg;


QStringList glaxnimate::io::raster::RasterFormat::extensions() const
{
    QStringList formats;
    for ( const auto& fmt : QImageReader::supportedImageFormats() )
        if ( fmt != "gif" && fmt != "webp" && fmt != "svg" )
            formats << QString::fromUtf8(fmt);
    return formats;
}

bool glaxnimate::io::raster::RasterFormat::on_open(QIODevice& dev, const QString&, model::Document* document, const QVariantMap&)
{
    auto bmp = document->assets()->images->values.insert(std::make_unique<model::Bitmap>(document));
    if ( auto file = qobject_cast<QFile*>(&dev) )
        bmp->filename.set(file->fileName());
    else
        bmp->data.set(dev.readAll());
    auto img = std::make_unique<model::Image>(document);
    img->image.set(bmp);
    QPointF p(bmp->pixmap().width() / 2.0, bmp->pixmap().height() / 2.0);
    img->transform->anchor_point.set(p);
    img->transform->position.set(p);
    document->main()->shapes.insert(std::move(img));
    document->main()->width.set(bmp->pixmap().width());
    document->main()->height.set(bmp->pixmap().height());
    return !bmp->pixmap().isNull();
}
